from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.urls import reverse_lazy

class XFrameOptionsSameOriginMixin:
    @xframe_options_sameorigin
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class Breadcrumb():
    def __init__(self, title, url="", reverse_name="", reverse_config=None):
        self.title = title
        self.url = url if url else reverse_lazy( reverse_name, reverse_config )


