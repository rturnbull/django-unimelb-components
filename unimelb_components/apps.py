from django.apps import AppConfig


class UnimelbComponentsConfig(AppConfig):
    name = 'unimelb_components'
