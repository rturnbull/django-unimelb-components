from django import template
from django.core.paginator import EmptyPage
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy

from unimelb_components.util import Breadcrumb

register = template.Library()


@register.simple_tag()
def unimelb_head_includes():
    """
    The necessary includes to have in the <head> element in order to use the Unimelb design system.
    """
    return mark_safe("""
        <link rel="stylesheet" href="https://dds-gen3.web.unimelb.edu.au/v8.1.5/uom.css">
        <script src="https://dds-gen3.web.unimelb.edu.au/v8.1.5/uom.js"></script>
    """)


@register.inclusion_tag('unimelb_components/paginator.html', takes_context=True)
def unimelb_paginator(context, adjacent_pages=3):
    """
    Inclusion tag for rendering a paginator for multi-page lists.

    Adapted from https://romanvm.pythonanywhere.com/post/bootstrap4-based-pagination-django-listview-30/

    Adds pagination context variables for use in displaying first, adjacent and
    last page links in addition to those created by the object_list generic
    view.

    :param context: parent template context
    :param adjacent_pages: the number of pages adjacent to the current
    :return: context for rendering paginator html code
    """
    start_page = max(context['page_obj'].number - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1
    end_page = context['page_obj'].number + adjacent_pages + 1
    if end_page >= context['paginator'].num_pages - 1:
        end_page = context['paginator'].num_pages + 1
    page_numbers = [n for n in range(start_page, end_page) if n in range(1, context['paginator'].num_pages + 1)]
    page_obj = context['page_obj']
    paginator = context['paginator']
    try:
        next_ = context['page_obj'].next_page_number()
    except EmptyPage:
        next_ = None
    try:
        previous = context['page_obj'].previous_page_number()
    except EmptyPage:
        previous = None
    return {
        'page_obj': page_obj,
        'paginator': paginator,
        'page': context['page_obj'].number,
        'pages': context['paginator'].num_pages,
        'page_numbers': page_numbers,
        'next': next_,
        'previous': previous,
        'has_next': context['page_obj'].has_next(),
        'has_previous': context['page_obj'].has_previous(),
        'show_first': 1 not in page_numbers,
        'show_last': context['paginator'].num_pages not in page_numbers,
        'request': context['request']
    }


@register.inclusion_tag('unimelb_components/messages.html', takes_context=True)
def unimelb_messages(context, dismiss=False):
    return {
        'messages': context.get('messages'),
        'dismiss': dismiss,
    }


@register.inclusion_tag('unimelb_components/staff_member_detailed.html')
def unimelb_staff_member_detailed(name, title=None, url=None, image=None, institution=None):
    institution = institution or "University of Melbourne"
    return {
        'name': name,
        'title': title,
        'url': url,
        'image': image,
        'institution':institution,
    }

@register.inclusion_tag('unimelb_components/staff_listing_detailed.html')
def unimelb_staff_listing_detailed(staff_list):
    return {
        'staff_list': staff_list,
    }

@register.inclusion_tag('unimelb_components/iframe.html')
def unimelb_iframe(url, height=400, caption=None):
    return {
        'url': url,
        'height': height,
        'caption': caption,
    }


@register.inclusion_tag('unimelb_components/search_form.html')
def unimelb_search_form(url=None):
    url = url or reverse_lazy("search")
    return {
        'url': url,
    }


@register.inclusion_tag('unimelb_components/breadcrumbs.html', takes_context=True)
def unimelb_breadcrumbs(context, home="Home", breadcrumbs=None, title=None):
    """
    Displays the breadcrumbs in the Unimelb header.

    title is the name of the current page (if this is not given, then it just shows the name for 'home' at the root)
    home is the name of the root page (default 'home')
    breadcrumbs is a list of the intermediate pages between the current page and the root. Use the unimelb_components.util.Breadcrumb class.
    Can be a single Breadcrumb object.

    If the values aren't given, then it tries to get them from the context.
    If can pick up the title from the context if a flatpage is used.
    """
    breadcrumbs = breadcrumbs or context.get('breadcrumbs') or []
    title = title or context.get('title')

    # If no title is found, then check the context for a flatpage
    if not title:
        flatpage = context.get('flatpage')
        if flatpage:
            title = flatpage.title

    # If there is just one parent given as a dictionary, then wrap it in a list
    if isinstance(breadcrumbs,dict) or isinstance(breadcrumbs,Breadcrumb):
        breadcrumbs = [breadcrumbs]

    position = len(breadcrumbs) + 1
    return {
        'home': home,
        'breadcrumbs': breadcrumbs,
        'position': position,
        'title': title,
    }


@register.inclusion_tag('unimelb_components/404.html')
def unimelb_404():
    return {}


@register.inclusion_tag('unimelb_components/500.html')
def unimelb_500():
    return {}

@register.filter
def verbose_name(value):
    return value._meta.verbose_name
