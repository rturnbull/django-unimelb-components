from django.test import TestCase
from django.template import Context, Template
from django.test import RequestFactory

from .models import TestModel
from .views import TestListView

class UnimelbPaginatorTest(TestCase):
    """
    Testing the Unimelb Paginator template tag.

    Taken part of code from here: https://krzysztofzuraw.com/blog/2017/how-to-test-django-template-tags
    """
    def setUp(self):
        for _ in range(100):
            TestModel().save()
        
    def test_paginator(self):
        request = RequestFactory().get('/')
        view = TestListView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'search-pagination')
        self.assertContains(response, "Showing items 1–10 of 100")
        self.assertContains(response, '<li class="act"><span>1</span></li>')

        # print(str(response.content))
