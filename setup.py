from setuptools import setup

requirements = [
    'Django>3',
]

setup(
    install_requires=requirements,
)