# django-unimelb-components

A Django extension add components from the University of Melbourne's template (see https://web.unimelb.edu.au).

## Usage
In your Django settings, add `unimelb_components` to your `INSTALLED_APPS`:
```
INSTALLED_APPS = [
    ...
    "unimelb_components",
    ...
]
```

In the templates, add this line to import the `unimelb_components` template tags:
```
{% load unimelb_components %}
```

## Messages
To use the [Django messages framework](https://docs.djangoproject.com/en/3.1/ref/contrib/messages/), add this tag at the position where you want messages to be shown:
```
{% unimelb_messages %}
```
This will display the message as a centred [Unimelb flash message](https://web.unimelb.edu.au/components/notices). If you are using Bootstrap and you wish to have a dismiss button, you can use this argument:
```
{% unimelb_messages dismiss=True %}
```
NB. The requirement for Bootstrap for the dismiss button will hopefully be removed in the future.


## Embeds
To embed an iframe, you can use this tag:

```
{% unimelb_iframe url="https://www.youtube.com/embed/nlF7qp5GNPI" %}
```

You can add a caption and explicitly set the height like this:
```
{% unimelb_iframe url="https://www.youtube.com/embed/nlF7qp5GNPI" height=560 caption="YouTube embed with caption." %}
```

If you need to use a URL from your project, you can use the name like this:
```
{% url "map_fullscreen" as map_fullscreen %}
{% unimelb_iframe url=map_fullscreen caption="Locations of people and works." %}
```

For more information on embedding, see https://web.unimelb.edu.au/components/embed

## Staff Lising

You can add a list of staff members to the context of a template in a view like this:
```
rob = dict(
    name="Robert Turnbull",
    url="https://findanexpert.unimelb.edu.au/profile/877006-robert-turnbull",
    image="https://pictures.staff.unimelb.edu.au/picture/thumbnail877006picture.jpg",
    title="Research Data Specialist",
)
paul = dict(
    name="Paul Tagell", 
    image="http://placeimg.com/200/200/animals",
    title="Philosopher",
    institution="Oxbridge"
)
context["people"] = [rob, paul]
```
If you do not explicitly state the institution, it is assumed to be the University of Melbourne.

Then in your template, you can add them as a detailed staff listing:
```
{% unimelb_staff_listing_detailed people %}
```

## Pagination
To add pagination, use this tag:
```
{% unimelb_paginator %}
```

The default for the number of adjacent pages that you want to show in the paginator is 3. You can adjust this number with the `adjacent_pages` argument.
```
{% unimelb_paginator adjacent_pages=4 %}
```

Credit: Elements adapted from https://romanvm.pythonanywhere.com/post/bootstrap4-based-pagination-django-listview-30/



## Authors
[Robert Turnbull](https://findanexpert.unimelb.edu.au/profile/877006-robert-turnbull)  
Melbourne Data Analytics Platform (MDAP)  
https://mdap.unimelb.edu.au  
University of Melbourne 